import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Teams from './page/pages/Teams';
import Team from './page/pages/Team';
import AddTeam from './page/pages/AddTeam';
import Nav from './page/Nav';
import Sidebar from './page/Sidebar';
import Container from './page/Container';

const App = () => (
    <div>
        <Nav />
        <Sidebar />
        <Container>
            <Switch>
                <Route exact path="/teams" component={Teams}/>
                <Route path="/teams/add" component={AddTeam}/>
                <Route path="/teams/:id" component={Team}/>
            </Switch>
        </Container>
    </div>
);

export default App;