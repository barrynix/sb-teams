import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import DocumentTitle from 'react-document-title';
import PageHeader from'../../ui/PageHeader';
import axios from "axios/index";

class TeamRow extends Component {
    render() {
        return (
            <tr key={this.props.team._id}>
                <td>{this.props.team.name}</td>
                <td className="text-center">0</td>
                <td className="text-center">0</td>
                <td className="text-center">{this.props.team.players.length}</td>
                <td>
                    <Link to={{ pathname: '/teams/' + this.props.team._id }}>
                        <button className="btn btn-primary btn-block">Team</button>
                    </Link>
                </td>
            </tr>
        );
    }
}

class Team extends Component {
    constructor(props) {
        super(props);

        this.state = {
            teams : []
        };
    }

    componentDidMount() {
        axios.get('http://localhost:3000/teams/')
        .then(res => {
            const teams = res.data;
            this.setState({ teams });
        });
    }
  render() {
    return (
        <div>
            <DocumentTitle title='All Teams'  />
            <PageHeader header={"Teams"} />
            <div className="col-md-8">
                <div className="form-group clearfix">
                    <Link to={{ pathname: '/teams/add' }}>
                        <button className="btn btn-success pull-right">
                            <i className="fa fa-plus"></i>&nbsp;Add Team
                        </button>
                    </Link>
                </div>
                <div className="panel panel-container">
                    <div className="panel-body">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th  className="text-center" style={{width: '100px'}}>Cases</th>
                                    <th  className="text-center" style={{width: '100px'}}>Total Cases</th>
                                    <th  className="text-center" style={{width: '100px'}}>Players</th>
                                    <th  className="text-center" style={{width: '150px'}}></th>
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.teams.map(team =>
                                <TeamRow team={team} />
                            )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

export default Team; 
