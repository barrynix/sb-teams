import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import DocumentTitle from 'react-document-title';
import axios from "axios/index";
import PageHeader from'../../ui/PageHeader';

class TeamPlayerRow extends Component {
    render() {
        return (
            <tr>
                <td>{this.props.player.name}</td>
                <td className="text-center">0</td>
                <td className="text-center">0</td>
                <td>
                    <Link to={{ pathname: '/users/' + this.props.player._id }}>
                        <button className="btn btn-primary btn-block">Player</button>
                    </Link>
                </td>
            </tr>
        );
    }
}

class Team extends Component {
    constructor(props) {
        super(props);

        this.state = {
            team_id : 0,
            team : {
                players: []
            }
        };
    }

    componentDidMount() {
        axios.get('http://localhost:3000/teams/' + this.props.match.params.id)
            .then(res => {
                const team = res.data;
                this.setState({ team });
            });
    }

    render() {
        return (
            <div>
                <DocumentTitle title={ this.state.team.name } />
                <PageHeader header={"Team: " + this.state.team.name} />
                <div className="col-md-6">
                    <div className="panel panel-container">
                        <div className="panel-body">
                            <h3 style={{ marginTop: "0px"}}>Team Info</h3>
                            <table className="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th  className="text-center" style={{width: '100px'}}>Cases</th>
                                    <th  className="text-center" style={{width: '100px'}}>Total Cases</th>
                                    <th  className="text-center" style={{width: '150px'}}></th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.team.players.map(player =>
                                    <TeamPlayerRow player={player} />
                                )}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Team; 
