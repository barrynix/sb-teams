import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import DocumentTitle from 'react-document-title';
import PageHeader from'../../ui/PageHeader';

class AddTeam extends Component {
    constructor(props) {
        super(props);

        this.state = {
            team_name: "",
            new_team_id : -1,
            redirect : false
        };

        this.addTeam = this.addTeam.bind(this);
        this.change = this.change.bind(this);
    }

    change(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    addTeam()
    {
        axios.post('http://localhost:3000/teams', {
            name:this.state.team_name
        })
        .then((response) => this.setState({ new_team_id : response.data._id }))
        .then(() => this.setState({ redirect : true }))
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={'/teams/' + this.state.new_team_id}/>;
        }

        return (
            <div>
                <DocumentTitle title='Add Team'  />
                <PageHeader header={"Add Team"} />
                <div className="col-md-6">
                    <div className="form-group">
                        <label>Team Names</label>
                        <input type="text" className="form-control" onChange={this.change} name="team_name"/>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-block btn-primary" onClick={this.addTeam}>ADD TEAM</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddTeam;
