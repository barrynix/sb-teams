import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
class Sidebar extends Component {
    render() {
        return (
            <div id="sidebar-collapse" className="col-sm-3 col-lg-2 sidebar">
                <ul className="nav menu">
                    <li><NavLink exact to="/" activeClassName="active"><em className="fa fa-dashboard">&nbsp;</em> Dashboard</NavLink></li>
                    <li><NavLink to="/teams" activeClassName="active"><em className="fa fa-users">&nbsp;</em> Teams</NavLink></li>
                    <li><NavLink to="/players" activeClassName="active"><em className="fa fa-user">&nbsp;</em> Players</NavLink></li>
                    <li><NavLink to="/logout" activeClassName="active"><em className="fa fa-power-off">&nbsp;</em> Logout</NavLink></li>
                </ul>
            </div>
        );
    }
}

export default Sidebar;
