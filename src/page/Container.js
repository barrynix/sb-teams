import React, { Component } from 'react';
class Container extends Component {
    render() {
        return (
            <div className="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
                {this.props.children}
            </div>
        );
    }
}

export default Container;
