import React, { Component } from 'react';
class TwoColPanel extends Component {
  render() {
    return (
      <div className="col-md-8">
        <div className="panel panel-container">
          <div className="row">
            <div className="col-xs-6 no-padding">
              <div className="panel panel-blue panel-widget border-right">
                <div className="row no-padding"><em className="fa fa-xl fa-users color-gray"></em>
                  <div className="large">{this.props.columns[0].count}</div>
                  <div className="text-muted">{this.props.columns[0].title}</div>
                </div>
              </div>
            </div>
            <div className="col-xs-6 no-padding">
              <div className="panel panel-blue panel-widget border-right">
                <div className="row no-padding"><em className="fa fa-xl fa-users color-teal"></em>
                  <div className="large">{this.props.columns[1].count}</div>
                  <div className="text-muted">{this.props.columns[1].title}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TwoColPanel;
