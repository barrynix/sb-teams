import React, { Component } from 'react';
class PageHeader extends Component {
    render() {
        return (
            <div className="col-md-12">
                <h1 className="page-header">{this.props.header}</h1>
            </div>
        );
    }
}

export default PageHeader;
